<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
        $data['firstname'] = $request->input('firstname');
        $data['lastname'] = $request->input('lastname');
        return view('welcome', $data);
    }
}
