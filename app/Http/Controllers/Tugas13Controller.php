<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Tugas13Controller extends Controller
{
    public function table()
    {
        $data['title'] = "Halaman Tabel";
        return view('pages.table', $data);
    }

    public function dataTable()
    {
        $data['title'] = "Halaman Data Tabel";
        return view('pages.data-table', $data);
    }
}
