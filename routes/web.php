<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Tugas13Controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home'])->name('home');
Route::get('/register', [AuthController::class, 'register'])->name('register');
Route::post('/welcome', [AuthController::class, 'welcome'])->name('welcome');

// Routing pada tugash hari ke 13
Route::get('/table', [Tugas13Controller::class, 'table'])->name('table');
Route::get('/data-tables', [Tugas13Controller::class, 'dataTable'])->name('data_table');

// Routing tugas hari ke 15
Route::get('/cast', [CastController::class, 'index'])->name('cast.index');
Route::get('/cast/create', [CastController::class, 'create'])->name('cast.create');
Route::post('/create', [CastController::class, 'store'])->name('cast.store');
Route::get('/cast/{cast_id}', [CastController::class, 'show'])->name('cast.show');
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit'])->name('cast.edit');
Route::put('/cast/{cast_id}', [CastController::class, 'update'])->name('cast.update');
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy'])->name('cast.destroy');