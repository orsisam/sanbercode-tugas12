<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>

	<h2>Sign Up Form</h2>

	<form action="{{ route('welcome') }}" method="POST">

        @csrf
		<label>First name:</label> <br>
		<input type="text" name="firstname"><br><br>

		<label>Last name:</label> <br>
		<input type="text" name="lastname"><br><br>

		<label for="">Gender:</label><br><br>
		<input type="radio" name="gender" id="">Male <br>
		<input type="radio" name="gender" id="">Female<br>
		<input type="radio" name="gender" id="">Other <br><br>

		<label for="">Nationality:</label><br><br>
		<select name="nationality" id="">
			<option value="indonesia">Indonesia</option>
			<option value="malaysia">Malaysia</option>
			<option value="singapore">Singapore</option>
			<option value="australia">Australia</option>
		</select><br><br>

		<label for="">Language Spoken:</label><br><br>
		<input type="checkbox" name="language" id="" value="indonesia">Bahasa Indonesia <br>
		<input type="checkbox" name="language" id="" value="english">English <br>
		<input type="checkbox" name="language" id="" value="other">Other <br><br>

		<label for="">Bio:</label><br><br>
		<textarea name="bio" id="" cols="30" rows="10"></textarea><br>

		<input type="submit" value="Sign Up">
	</form>
</body>
</html>