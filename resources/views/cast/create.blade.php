@extends('layout.master')

@section('title', 'Buat Cast')

@section('page_title', 'Tambah Cast Baru')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Tambah Cast Baru</h3>

            <div class="card-tools">
                <a href="{{ route('cast.index') }}" class="btn btn-sm btn-secondary">
                    <i class="nav-icon fas fa-arrow-left"> Kembali</i>
                </a>
            </div>
        
        <div class="card-body">
           
            <form action="{{ route('cast.store') }}" method="POST">
                 @csrf
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="masukkan Nama Cast">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>                        
                    @enderror
                </div>
                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="text" class="form-control" id="umur" name="umur" placeholder="Masukkan Umur Cast">
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <textarea name="bio" id="bio" rows="10" class="form-control" placeholder="Masukkan Biografi Cast"></textarea>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fas fa-save"> Simpan</i></button>
            </form>
        </div>
    </div>
@endsection