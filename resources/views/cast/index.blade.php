@extends('layout.master')

@section('title', 'Cast')

@section('page_title', 'Halaman Cast')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Halaman Cast</h3>

            <div class="card-tools">
                <a href="{{ route('cast.create') }}" class="btn btn-sm btn-primary">
                    <i class="nav-icon fas fa-plus"> Tambah</i>
                </a>
            </div>
        </div>
        {{-- /.card-header --}}
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th scope="col" width="3%">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Umur</th>
                        <th scope="col" width="17%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($cast as $key=>$value)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $value->nama }}</td>
                            <td>{{ $value->umur }}</td>
                            <td>
                                <a href="{{ route('cast.show', ['cast_id'=>$value->id]) }}" class="btn btn-sm btn-info">Show</a>
                                <a href="{{ route('cast.edit', ['cast_id'=>$value->id]) }}" class="btn btn-sm btn-primary">Edit</a>
                                <form action="{{ route('cast.destroy', ['cast_id'=>$value->id]) }}" method="POST" style="display: inline">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-sm btn-danger">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr colspan="3">
                            <td>No data</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    
@endsection

@push('scripts')
    <script src="../../plugins/datatables/jquery.dataTables.js"></script>
    <script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush