@extends('layout.master')

@section('title', 'Cast')

@section('page_title', 'Show Cast')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Show Cast {{ $cast->id }}</h3>

            <div class="card-tools">
                <a href="{{ route('cast.index') }}" class="btn btn-sm btn-secondary">
                    <i class="nav-icon fas fa-arrow-left"> Kembali</i>
                </a>
            </div>
        </div>
        {{-- /.card-header --}}
        <div class="card-body">
            
                <h4>{{ $cast->nama }}</h4>
                <p><strong>Umur</strong>: {{ $cast->umur }}</p>
                <p><strong>Biografi: </strong>{{ $cast->bio }}</p>
            
        </div>
    </div>
    
@endsection